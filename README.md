![](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/elio-sassy-fibonacciness-logo.png)

> The golden ratio is the most irrational number **Richard A. Dunlap**

# SassyFibonacciness ![release](https://elioway.gitlab.io/eliosin/icon/devops/release/favicon.ico "release")

Use the famous Fibonacci sequence to naturally size and weight ranges for your SASS stylesheets.

- [sassy-fibonacciness Documentation](https://elioway.gitlab.io/eliosin/sassy-fibonacciness)
- [sassy-fibonacciness Demo](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/demo.html)

## Seeing is believing

```shell
npm run build
gulp
```

## Installing

```
npm install @elioway/sassy-fibonacciness
yarn add @elioway/sassy-fibonacciness
```

Import the functions into your master scss file.

```
@import "@elioway/sassy-fibonacciness/dist/SassyFibonacciness";
```

- [Installing sassy-fibonacciness](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/installing.html)

## Nutshell

### `npm test`

### `npm run prettier`

- [sassy-fibonacciness Quickstart](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/quickstart.html)
- [sassy-fibonacciness Credits](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/credits.html)

![](https://elioway.gitlab.io/eliosin/sassy-fibonacciness/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
