# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/eliosin/sassy-fibonacciness.git
```

## Build

```shell
npm run build
npm run test
```
