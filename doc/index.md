<aside>
  <dl>
  <dd>Then feed on thoughts, that voluntary move</dd>
  <dd>Harmonious numbers</dd>
</dl>
</aside>

Use the famous Fibonacci sequence to naturally size and weight ranges for your SASS stylesheets.

On modern webpages plenty of things need scaling incrementally. Font sizes increase; spaces decrease on smaller screens; feature boxs shrink with quantity.

**SassyFibonacciness** makes use of nature's natural scale to provide values you can use for CSS size properties. One of the principals of **the elioWay** is a "you handle this for me" attitude... inviting developers to surrender minute control in the pursuit of simplicity.
