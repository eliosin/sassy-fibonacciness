# SassyFibonacciness Quickstart

- [SassyFibonacciness Prerequisites](/eliosin/sassy-fibonacciness/prerequisites.html)
- [Installing SassyFibonacciness](/eliosin/sassy-fibonacciness/installing.html)

## Nutshell

1. Install and import into your SCSS build file.
2. Make a skiponacci list based on Fibonacci numbers.
3. Use the sequence as a source of size values in your styles and media widths.
4. Convert the skiponacci to a fontonacci list of weighted percents you can use for heading `font-size`.

## Skiponacci

`skiponacci` is a function for generating Fibonacci number sequences. As well as the classic, you can change the _fibonacciness_ of the seq to create more shallow or acute curves:

```
skiponacci($target_len: 6, $fibonacciness: 1);
```

- parameter: **\$target_len**

  - How many numbers you need in the seq.

- parameter: **\$fibonacciness**

  - 0 just doubles the number... steeply increased by 200%.
  - 1 is the standard golden ratio Fibonacci. _Default._
  - 2 or more produces shallower Fibonacci sequences.

### Examples

```
@debug skiponacci(10);
1 2 3 5 8 13 21 34 55 89

@debug skiponacci(10, 0);
1 2 4 8 16 32 64 128 256 512

@debug skiponacci(10, 1);
1 2 3 5 8 13 21 34 55 89

@debug skiponacci(10, 2);
1 2 3 4 6 9 13 19 28 41

@debug skiponacci(10, 3);
1 2 3 4 5 7 10 14 19 26

@debug skiponacci(10, 4);
1 2 3 4 5 6 8 11 15 20
```

### Usage

```
$paddings: skiponacci(6);
1 2 3 5 8 13 21 34 55 89

.sm-pad {
  padding: nth($paddings, 3) * 1px;
}
.md-pad {
  padding: nth($paddings, 4) * 1px;
}
.lg-pad {
  padding: nth($paddings, 5) * 1px;
}
.xl-pad {
  padding: nth($paddings, 6) * 1px;
}
```

## Fontonacci

`fontonacci` is a function which redistributes Skiponacci seqs into font-sizes. The sequence's list is usefully reversed so hX==list(X).

```
fontonacci($skippy: 1 2 3 5 8 13, $target_min: 100%, $target_max: 500%);
```

- parameter: **\$skippy**

  - The Fibonacci seq you generated with `skiponacci`.

- parameter: **\$target_min**

  - The smallest font size you want, usually aligned with h6 at 100%.

- parameter: **\$target_max**

  - The largest font size you want, usually aligned with h1 at some large %.

### Examples

```
@debug fontonacci(skiponacci(6), 100%, 500%);
500% 294% 190% 139% 113% 100%

@debug fontonacci(skiponacci(6, 4), 100%, 500%);
500% 420% 340% 260% 180% 100%

@debug fontonacci(skiponacci(6), 1vw, 4vw);
4vw 2.8vw 2vw 1.5vw 1.3vw 1vw
```

### Usage

```
$skipi: skiponacci(6);
$fonti: fontonacci($skipi, $target_min: 100%, $target_max: 500%);

h1 {
  font-size: nth($fonti, 1);
}
h2 {
  font-size: nth($fonti, 2);
}
```
